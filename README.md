# MantisBT docker image

## Unoffical docker image for the Mantis Bug Tracker

Docker image location: hub.docker.com/repository/docker/minecraftchest1/mantisbt

### Volumes

| Path | Description |
------|---------------
| `/data` | Contains log files and user uploads. |
| `/var/www/mantisbt/plugins` | Plugins directory. |
| `/var/www/mantisbt/config` | 
