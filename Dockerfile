# syntax=docker/dockerfile:1

FROM debian:stable-slim
RUN apt-get update && apt-get upgrade -y

RUN apt-get -y install nginx php-fpm php-mysql php-pgsql php-mbstring php-curl php-gd curl php-ldap

RUN curl -L 'https://downloads.sourceforge.net/project/mantisbt/mantis-stable/2.25.2/mantisbt-2.25.2.tar.gz?ts=gAAAAABhps1SBnDWX3qXu_rkSU96SfnM-IlddlHMLjDFSt_C2GJ1onW-PBOZKO2JsVMX1ki5TYLJGs_Ypaui9RiHCVk67M6OAg%3D%3D&r=https%3A%2F%2Fsourceforge.net%2Fprojects%2Fmantisbt%2Ffiles%2Fmantis-stable%2F2.25.2%2Fmantisbt-2.25.2.tar.gz%2Fdownload%3Fuse_mirror%3Dnewcontinuum%26use_mirror%3Dnewcontinuum%26r%3Dhttps%253A%252F%252Fsourceforge.net%252Fprojects%252Fmantisbt%252Ffiles%252Fmantis-stable%252F2.25.2%252F'	--output /tmp/mantisbt.tar.gz && tar -xf /tmp/mantisbt.tar.gz -C /var/www/ && mv /var/www/mantisbt-* /var/www/mantisbt && chmod ug+rw /var/www/mantisbt/ -R && chown www-data:www-data /var/www/mantisbt/ -R &&	rm /etc/nginx/sites-enabled/* && mkdir /run/php/ && touch /run/php/php7.4-fpm.sock

RUN mkdir /data /data/logs /data/uploads && touch /data/logs/mantis.log
RUN chmod ugo+rw /data -R && chown www-data:www-data /data -R
	
COPY ./entry.sh /entry.sh
COPY ./nginx.default /etc/nginx/sites-enabled/default
RUN chmod ugo+rwx /entry.sh

CMD /entry.sh
